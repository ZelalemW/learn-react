import React from 'react'
import PropTypes from 'prop-types'
import {Book} from './Book'
import {Hiring} from './Hiring'
import {NotHiring} from './NotHiring'
import '../lib/bootstrap.min.css';

class Library extends React.Component {
    static defaultProps = {
        books: [
            { title: 'Neural network', author: 'Endalew Wubale', pages: 1000, description: 'Practical neural network for .NET developers' }
        ]
    }

    state = {
        open: true,
        freeBookmark: true,
        hiring: false,
        data: [],
        loading: false
    }

    componentDidMount() {
        this.setState({ loading: true })
        fetch('https://hplussport.com/api/products/order/price/sort/asc/qty/1')
            .then(data => data.json())
            .then(data => this.setState({ data, loading: false }))
    }

    toggleOpenClosed = () => {
        this.setState(prevState => ({
            open: !prevState.open
        }))
    }

    toggleOpenClosed() {
        console.log('toggling...')
        this.setState(prevState => ({
            open: !prevState.open
        }))
    }

    render() {
        console.log(this.state)
        const { books } = this.props
        return (
            <div>
                {this.state.hiring ? <Hiring /> : <NotHiring />}
                {this.state.loading
                    ? "Loading..."
                    : <div>
                        {this.state.data.map(product => {
                            return (
                                <div key={product.id}>
                                    <h3>Library Products of the week</h3>
                                    <h4>{product.name}</h4>
                                    <img alt={product.name} src={product.image} height={100} />
                                </div>
                            )
                        })}
                    </div>
                }
                <h1>The library is {this.state.open ? 'open' : 'closed'}</h1>
                <button onClick={this.toggleOpenClosed} className="btn btn-primary"> Change</button>
                {books.map(
                    (book, i) =>
                        <Book key={i}
                            title={book.title}
                            author={book.author}
                            pages={book.pages}
                            description={book.description}
                            freeBookmark={this.state.freeBookmark} />
                )}
            </div>
        )
    }
}


Library.propTypes = {
    books: PropTypes.array
}

Book.prototype = {
    title: PropTypes.string,
    author: PropTypes.string,
    pages: PropTypes.number,
    description: PropTypes.description,
    freeBookmark: PropTypes.bool
}

export default Library